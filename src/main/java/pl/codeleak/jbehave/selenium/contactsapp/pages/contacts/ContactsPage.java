package pl.codeleak.jbehave.selenium.contactsapp.pages.contacts;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.*;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractPage;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ConfirmationAlert;
import pl.codeleak.jbehave.selenium.contactsapp.support.ApplicationProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class ContactsPage extends AbstractPage {

    private ContactFormDialog contactFormDialog;
    private ContactDetailsDialog contactDetailsDialog;
    private ConfirmationAlert confirmationAlert;

    public ContactsPage(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public int size() {
        var rows = findElements(By.xpath("//table/tbody/tr//span[contains(@data-property, 'contact-')]/ancestor::tr"));
        return rows.size();
    }

    public boolean hasNoResults() {
        return size() == 0;
    }

    public String noResultsText() {
        return findElement(By.xpath("//table/tbody/tr/td/div")).getText();
    }

    public ContactsPage search(String searchTerm) {
        var element = findElement(By.id("search-filter"));
        element.clear();
        element.sendKeys(searchTerm);
        return this;
    }

    public Pagination getPagination() {
        return new Pagination(getDriverProvider());
    }

    public ContactFormDialog openNewContactFormDialog() {
        findElement(By.xpath("//button//*[contains(text(), 'New Contact')]")).click();
        this.contactFormDialog = new ContactFormDialog(getDriverProvider());
        return contactFormDialog();
    }

    public ContactsPage toggleSelectAll() {
        findElement(By.cssSelector("table thead tr:nth-child(1) th:nth-child(1)")).click();
        return this;
    }

    public ContactsPage toggleSelectContact(String contactName) {
        // click checkbox-column in contact row
        findElement(By.xpath("//table/tbody//span[contains(text(), '" + contactName + "')]/ancestor::tr//td[@class = 'checkbox-column']")).click();
        return this;
    }

    public ContactsPage clickDownloadContacts() {
        findElement(By.xpath("//button//div//i[contains(text(), 'get_app')]/ancestor::button")).click();
        return this;
    }

    public ConfirmationAlert clickDeleteContact(String contactName) {
        findElement(By.xpath("//table/tbody//span[contains(text(), '" + contactName + "')]/ancestor::tr//i[text() = 'delete']")).click();
        this.confirmationAlert = new ConfirmationAlert(switchTo().alert());
        return confirmationAlert();
    }

    public ConfirmationAlert clickDeleteAllSelectedContacts() {
        findElement(By.xpath("//button//div//i[contains(text(), 'delete')]/ancestor::button")).click();
        this.confirmationAlert = new ConfirmationAlert(switchTo().alert());
        return confirmationAlert();
    }

    public ContactFormDialog openEditContactFormDialog(String contactName) {
        findElement(By.xpath("//table/tbody//span[contains(text(), '" + contactName + "')]/ancestor::tr//i[text() = 'edit']")).click();
        this.contactFormDialog = new ContactFormDialog(getDriverProvider());
        return contactFormDialog();
    }

    public ContactDetailsDialog openContactDetailsDialog(String contactName) {
        findElement(By.xpath("//table/tbody//span[contains(text(), '" + contactName + "')]/ancestor::tr//span[@data-property='contact-name']")).click();
        this.contactDetailsDialog = new ContactDetailsDialog(getDriverProvider());
        return contactDetailsDialog();
    }

    public List<String> getContactsNames() {
        return findElements(By.cssSelector("td span[data-property=contact-name]"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getContactsEmails() {
        return findElements(By.cssSelector("td span[data-property=contact-email]"))
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public List<String> getDownloadedFileContents() {
        Path downloadDir = ApplicationProperties.getDownloadDirectory();
        Path contactsFile = webDriverWait().until(d -> {
            try {
                return Files.list(downloadDir).filter(path -> path.endsWith("contacts.csv")).findFirst().orElse(null);
            } catch (IOException e) {
                return null;
            }
        });

        try {
            return Files.readAllLines(contactsFile);
        } catch (IOException e) {
            return List.of();
        }
    }

    public ContactFormDialog contactFormDialog() {
        return contactFormDialog;
    }

    public ContactDetailsDialog contactDetailsDialog() {
        return contactDetailsDialog;
    }

    public ConfirmationAlert confirmationAlert() {
        return confirmationAlert;
    }
}

