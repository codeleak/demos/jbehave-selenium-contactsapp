package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface Component {

    void clearAndSendKeys(WebElement webElement, String value);

    boolean isElementPresent(By locator);

    boolean isElementVisible(By locator);

    WebDriverWait webDriverWait();

}
