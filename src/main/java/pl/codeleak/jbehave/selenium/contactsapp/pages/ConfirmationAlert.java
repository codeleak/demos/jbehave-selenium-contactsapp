package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.openqa.selenium.Alert;

public class ConfirmationAlert {

    private final Alert alert;

    public ConfirmationAlert(Alert alert) {
        this.alert = alert;
    }

    public String message() {
        return alert.getText();
    }

    public void accept() {
        alert.accept();
    }
}
