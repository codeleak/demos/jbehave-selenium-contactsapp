package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class Snackbar extends AbstractComponent {

    private final WebElement snackbar;

    public Snackbar(WebDriverProvider driverProvider) {
        super(driverProvider);
        snackbar = webDriverWait().until(visibilityOfElementLocated(By.id("snackbar")));
        webDriverWait().until(driver -> !snackbar.getText().isEmpty());
    }

    public String getText() {
        return snackbar.getText();
    }
}
