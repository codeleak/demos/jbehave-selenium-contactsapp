package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class AbstractDialog extends AbstractComponent implements Dialog {

    protected final WebElement dialog;
    private final By dialogLocator = By.cssSelector("#app > div.v-dialog__content.v-dialog__content--active");

    public AbstractDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
        this.dialog = webDriverWait().until(visibilityOfElementLocated(dialogLocator));
    }

    @Override
    public boolean isVisible() {
        // re-check visibility
        return isElementPresent(dialogLocator) && isElementVisible(dialogLocator);
    }

    @Override
    public String getDialogTitle() {
        return dialog.findElement(By.cssSelector("div.v-card__title.headline.grey.lighten-2.v-card__title--primary")).getText();
    }

    @Override
    public void closeDialog() {
        dialog.findElement(By.cssSelector("div.v-card__actions > button > div")).click();
        webDriverWait().until(invisibilityOfElementLocated(dialogLocator));
    }
}
