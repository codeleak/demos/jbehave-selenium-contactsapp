package pl.codeleak.jbehave.selenium.contactsapp.support;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.*;
import org.jbehave.core.embedder.executors.DirectExecutorService;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.reporters.*;
import org.jbehave.core.steps.*;
import org.jbehave.web.selenium.*;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ContactsApp;
import pl.codeleak.jbehave.selenium.contactsapp.steps.*;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static org.jbehave.core.reporters.Format.*;
import static org.jbehave.web.selenium.WebDriverHtmlOutput.WEB_DRIVER_HTML;

public class WebDriverEmbedder extends Embedder {

    private final WebDriverProvider driverProvider = new ManagedWebDriverProvider();
    private final ContactsApp contactsApp = new ContactsApp(driverProvider);
    private final WebDriverSteps lifecycleSteps = new PerStoriesWebDriverSteps(driverProvider);

    @Override
    public EmbedderControls embedderControls() {
        return new EmbedderControls()
                .doIgnoreFailureInView(false)
                .doVerboseFailures(true);
    }

    @Override
    public ExecutorService executorService() {
        // required by org.jbehave.web.selenium.PerStoriesWebDriverSteps
        return new DirectExecutorService().create(embedderControls());
    }


    @Override
    public List<String> metaFilters() {
        // https://jbehave.org/reference/latest/meta-filtering.html
        // Mark scenario or Story with @skip to skip the execution
        return List.of("-skip");
    }

    @Override
    public Configuration configuration() {
        return new SeleniumConfiguration()
                .useWebDriverProvider(driverProvider)
                .useStoryControls(new StoryControls()
                        .doSkipScenariosAfterFailure(true)
                        .doResetStateBeforeScenario(true)
                        .doResetStateBeforeStory(true))
                .useStoryLoader(new LoadFromClasspath())
                .useParameterConverters(new ParameterConverters()
                        .addConverters(new ParameterConverters.DateConverter(new SimpleDateFormat("yyyy-MM-dd"))))
                .usePendingStepStrategy(new FailingUponPendingStep())
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withFormats(CONSOLE, TXT, STATS, WEB_DRIVER_HTML)
                        .withPathResolver(new FilePrintStreamFactory.ResolveToPackagedName())
                        .withFailureTrace(true)
                        .withFailureTraceCompression(false)
                        .withCrossReference(new CrossReference()));
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(
                configuration(),
                new HomePageSteps(contactsApp),
                new LoginSteps(contactsApp),
                new ContactsPageSteps(contactsApp),
                lifecycleSteps);
    }
}
