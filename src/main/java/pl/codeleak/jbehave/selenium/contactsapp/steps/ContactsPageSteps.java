package pl.codeleak.jbehave.selenium.contactsapp.steps;

import org.jbehave.core.annotations.*;
import org.jbehave.core.model.ExamplesTable;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ContactsApp;
import pl.codeleak.jbehave.selenium.contactsapp.pages.contacts.*;
import pl.codeleak.jbehave.selenium.contactsapp.support.ApplicationProperties;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ContactsPageSteps {

    private final ContactsApp contactsApp;
    private ContactsPage contactsPage;

    public ContactsPageSteps(ContactsApp contactsApp) {
        this.contactsApp = contactsApp;
    }

    @BeforeScenario
    public void removePreviouslyDownloadedFiles() {
        try {
            var downloadedFiles = Files.list(ApplicationProperties.getDownloadDirectory())
                    .filter(p -> p.toString().matches(".*contacts.*.csv"))
                    .collect(Collectors.toUnmodifiableList());
            for (Path filePath : downloadedFiles) {
                Files.delete(filePath);
            }
        } catch (IOException ignored) {
        }
    }

    //
    // Given
    //

    @Given("there are no contacts")
    public void isAtContactsPageWithNoContacts() {
        contactsPage = contactsApp.navigateTo()
                .setContacts("/empty.json")
                .openLoginDialog()
                .username("contacts")
                .password("demo")
                .login()
                .contactsPage();
    }

    @Given("there are contacts as described in '$resourceName'")
    public void isAtContactsPageWithContactsLoadedFromResource(String resourceName) {
        contactsPage = contactsApp
                .navigateTo()
                .setContacts(resourceName)
                .openLoginDialog()
                .username("contacts")
                .password("demo")
                .login()
                .contactsPage();
    }

    //
    // Browsing contacts
    //

    @When("user toggles select all contacts")
    public void toggleSelectAllContacts() {
        contactsPage.toggleSelectAll();
    }

    @When("user toggles select contact with name '$name'")
    public void toggleSelectContact(String name) {
        contactsPage.toggleSelectContact(name);
    }

    @When("user enters '$impossible_to_find' in search")
    public void enterSearchTerm(String value) {
        contactsPage.search(value);
    }

    @Then("user sees no results message: '$message'")
    public void noContacts(String message) {
        assertThat(contactsApp.contactsPage().hasNoResults(), is(true));
        assertThat(contactsApp.contactsPage().noResultsText(), is(message));
    }

    @Then("user should see exactly $count contacts")
    public void contactsListSizeIs(int count) {
        assertThat(contactsPage.size(), is(count));
    }

    @Then("user should see contact with name '$value'")
    public void contactWithNameIsOnTheList(String value) {
        assertThat(contactsPage.getContactsNames(), hasItem(value));
    }

    @Then("user should not see contact with name '$value'")
    public void contactWithNameIsNotOnTheList(String value) {
        assertThat(contactsPage.getContactsNames(), not(hasItem(value)));
    }

    @Then("user should see contact with email '$value'")
    public void contactWithEmailIsOnTheList(String name) {
        assertThat(contactsPage.getContactsEmails(), hasItem(name));
    }

    @Then("user should not see contact with email '$value'")
    public void contactWithEmailIsNotOnTheList(String name) {
        assertThat(contactsPage.getContactsEmails(), not(hasItem(name)));
    }

    @Then("user should see contact names specified by: $table")
    public void contactsNamesAreOnTheList(ExamplesTable table) {
        var expectedNames = table.getRows().stream().map(row -> row.get("name")).collect(Collectors.toUnmodifiableList());
        var actualNames = contactsPage.getContactsNames();
        assertThat(actualNames, containsInAnyOrder(expectedNames.toArray()));
    }

    @Then("user should see contact emails specified by: $table")
    public void contactsEmailsAreOnTheList(ExamplesTable table) {
        var expectedEmails = table.getRows().stream().map(row -> row.get("email")).collect(Collectors.toUnmodifiableList());
        var actualNames = contactsPage.getContactsEmails();
        assertThat(actualNames, containsInAnyOrder(expectedEmails.toArray()));
    }

    @Then("user should see contact names and emails specified by: $table")
    public void contactsNamesAndEmailsAreOnTheList(ExamplesTable table) {
        contactsNamesAreOnTheList(table);
        contactsEmailsAreOnTheList(table);
    }

    //
    // Details
    //

    @When("user clicks name of contact '$name'")
    public void clickContactName(String name) {
        contactsPage.openContactDetailsDialog(name);
    }

    @Then("contact details dialog is visible")
    public void contactDetailsDialogIsVisible() {
        assertThat(contactsPage.contactDetailsDialog().isVisible(), is(true));
    }

    @Then("user sees contact name: '$name'")
    public void contactNameIs(String name) {
        assertThat(contactsPage.contactDetailsDialog().getName(), equalTo(name));
    }

    @Then("user sees contact email '$email' next to email icon")
    public void contactEmailIs(String email) {
        assertThat(contactsPage.contactDetailsDialog().getEmail(), equalTo(email));
    }

    @Then("user sees contact phone '$phone' next to phone icon")
    public void contactPhoneIs(String phone) {
        assertThat(contactsPage.contactDetailsDialog().getPhone(), equalTo(phone));
    }

    //
    // Pagination
    //

    @When("user selects to see '$rowsPerPage' records")
    public void selectRowsPerPage(String rowsPerPage) {
        contactsPage.getPagination().rowsPerPage(rowsPerPage);
    }

    @When("user navigates to next page")
    public void navigateToNextPage() {
        contactsPage.getPagination().nextPage();
    }

    @When("user navigates to previous page")
    public void navigateToPreviousPage() {
        contactsPage.getPagination().previousPage();
    }

    @Then("next page is enabled")
    public void nextPageIsEnabled() {
        assertThat(contactsPage.getPagination().hasNextPage(), is(true));
    }

    @Then("previous page is enabled")
    public void previousPageIsEnabled() {
        assertThat(contactsPage.getPagination().hasPreviousPage(), is(true));
    }

    @Then("next page is disabled")
    public void nextPageIsDisabled() {
        assertThat(contactsPage.getPagination().hasNextPage(), is(false));
    }

    @Then("previous page is disabled")
    public void previousPageIsDisabled() {
        assertThat(contactsPage.getPagination().hasPreviousPage(), is(false));
    }

    //
    // Contact Form (new contact, edit contact)
    //

    @When("user opens new contact form dialog")
    public void openNewContactForm() {
        contactsPage.openNewContactFormDialog();
    }

    @When("user opens edit contact form for '$name'")
    public void openEditContactForm(String name) {
        contactsPage.openEditContactFormDialog(name);
    }

    @When("user enters name: '$value'")
    public void userEntersName(String value) {
        contactsPage.contactFormDialog().setName(value);
    }

    @When("user enters email: '$value'")
    public void entersEmail(String value) {
        contactsPage.contactFormDialog().setEmail(value);
    }

    @When("user enters phone: '$value'")
    public void entersPhone(String value) {
        contactsPage.contactFormDialog().setPhone(value);
    }

    @When("user enters notes: '$value'")
    public void entersNotes(String value) {
        contactsPage.contactFormDialog().setNotes(value);
    }

    @When("user adds label: '$value'")
    public void addLabel(String value) {
        contactsPage.contactFormDialog().addLabel(value);
    }

    @When("user uploads avatar: '$resourceName'")
    public void setAvatar(String resourceName) {
        contactsPage.contactFormDialog().setAvatar(resourceName);
    }

    @When("user saves the form")
    public void save() {
        contactsPage.contactFormDialog().save();
    }

    @Then("contact form dialog should be closed")
    public void contactFormDialogIsNotVisible() {
        assertThat(contactsPage.contactFormDialog().isVisible(), is(false));
    }

    @Then("user sees name: '$value' in the form")
    public void nameHasValue(String value) {
        assertThat(contactsPage.contactFormDialog().getContactName(), equalTo(value));
    }

    @Then("user sees email: '$value' in the form")
    public void emailHasValue(String value) {
        assertThat(contactsPage.contactFormDialog().getContactEmail(), is(value));
    }

    @Then("user sees avatar: '$value' in the form")
    public void avatarHasValue(String value) {
        assertThat(contactsPage.contactFormDialog().getAvatarImage(), containsString(value));
    }

    @Then("user sees '$error' error in the contact form")
    public void formError(String error) {
        assertThat(contactsPage.contactFormDialog().getErrorMessages(), hasItem(error));
    }

    //
    // Download
    //

    @When("user clicks download contacts")
    public void clickDownloadContacts() {
        contactsPage.clickDownloadContacts();
    }

    @Then("downloaded file should contain lines specified by: $table")
    public void downloadedFileContains(ExamplesTable table) {
        var expectedLines = table.getRows().stream().map(row -> row.get("name")).collect(Collectors.toUnmodifiableList());
        var actualLines = contactsPage.getDownloadedFileContents();
        assertThat(actualLines, containsInAnyOrder(expectedLines.toArray()));
    }

    //
    // Delete
    //

    @When("user clicks delete icon for contact with name '$name'")
    public void clickDeleteContact(String name) {
        contactsPage.clickDeleteContact(name);
    }

    @When("user clicks delete selected contacts")
    public void clickDeleteAllSelectedContacts() {
        contactsPage.clickDeleteAllSelectedContacts();
    }

    @Then("user sees alert with message '$message'")
    public void alertWithMessageIsDisplayed(String message) {
        assertThat(contactsPage.confirmationAlert().message(), equalTo(message));
    }

    @When("user confirms deletion")
    public void acceptAlertForDeletion() {
        contactsPage.confirmationAlert().accept();
    }
}
