package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import pl.codeleak.jbehave.selenium.contactsapp.pages.contacts.ContactsPage;
import pl.codeleak.jbehave.selenium.contactsapp.pages.login.LoginDialog;
import pl.codeleak.jbehave.selenium.contactsapp.pages.signup.SignupDialog;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;

/**
 * Application Entry Point. This is where the user journey starts.
 */
public class ContactsApp extends AbstractPage {

    public ContactsApp(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public ContactsApp navigateTo() {
        get(baseUrl());
        return this;
    }

    /**
     * Load contacts from JSON resource. Call this method before logging it to the application.
     *
     * @param resourceName name of the classpath resource
     * @return ContactsApp
     */
    public ContactsApp setContacts(String resourceName) {
        try {
            Path path = Paths.get(getClass().getResource(resourceName).toURI());
            String json = Files.readString(path);
            executeScript("window.localStorage.clear(); window.localStorage.setItem('contacts-app-data', arguments[0])", json);
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Can't set the contacts with localStorage", e);
        }
        return this;
    }

    public String getHeading() {
        return findElement(By.cssSelector("#home h1")).getText();
    }

    public String getSubHeading() {
        return findElement(By.cssSelector("#home h1 + p")).getText();
    }

    public LoginDialog openLoginDialog() {
        findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Log in')]")).click();
        return new LoginDialog(getDriverProvider());
    }

    public SignupDialog openSignupDialog() {
        findElement(By.xpath("//*[@id='home']//div[contains(@class, 'v-card__text')]//button/div[contains(text(), 'Signup')]")).click();
        return new SignupDialog(getDriverProvider());
    }

    public ContactsPage contactsPage() {
        return new ContactsPage(getDriverProvider());
    }
}
