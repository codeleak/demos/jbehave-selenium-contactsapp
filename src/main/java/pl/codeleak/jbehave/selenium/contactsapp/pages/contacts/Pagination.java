package pl.codeleak.jbehave.selenium.contactsapp.pages.contacts;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractComponent;

public class Pagination extends AbstractComponent {

    private final By nextPageLocator = By.cssSelector("button[aria-label='Next page']");
    private final By previousPageLocator = By.cssSelector("button[aria-label='Previous page']");

    public Pagination(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public Pagination rowsPerPage(String rowsPerPage) {
        findElement(By.cssSelector(".v-datatable__actions .v-datatable__actions__select .v-input__slot")).click();
        findElement(By.xpath("//div[contains(@class, 'v-select-list')]//div[text() = '" + rowsPerPage + "']/ancestor::a")).click();
        return this;
    }

    public boolean hasNextPage() {
        return isElementPresent(nextPageLocator)
                && isElementVisible(nextPageLocator)
                && findElement(nextPageLocator).isEnabled();
    }

    public Pagination nextPage() {
        findElement(nextPageLocator).click();
        return this;
    }


    public boolean hasPreviousPage() {
        return isElementPresent(previousPageLocator)
                && isElementVisible(previousPageLocator)
                && findElement(previousPageLocator).isEnabled();
    }

    public Pagination previousPage() {
        findElement(previousPageLocator).click();
        return this;
    }

    public ContactsPage getContactsPage() {
        return new ContactsPage(getDriverProvider());
    }

}
