package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import pl.codeleak.jbehave.selenium.contactsapp.support.ApplicationProperties;

public abstract class AbstractPage extends AbstractComponent implements Page {

    public AbstractPage(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    @Override
    public String baseUrl() {
        return ApplicationProperties.getBaseUrl();
    }

    public String getToolbarTitle() {
        return findElement(By.cssSelector("nav div.v-toolbar__title")).getText();
    }

    public Footer getFooter() {
        return new Footer(getDriverProvider());
    }

    public ContactsApp logout() {
        findElement(By.cssSelector("#app-nav > div.v-toolbar__content > button")).click();
        return new ContactsApp(getDriverProvider());
    }

    public AboutDialog openAboutDialog() {
        var aboutButton = findElement(By.cssSelector("#app-nav > div.v-toolbar__content button"));
        aboutButton.click();
        return new AboutDialog(getDriverProvider());
    }

    public Snackbar snackbar() {
        return new Snackbar(getDriverProvider());
    }

}
