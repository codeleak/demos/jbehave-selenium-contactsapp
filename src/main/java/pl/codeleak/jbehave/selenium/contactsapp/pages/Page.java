package pl.codeleak.jbehave.selenium.contactsapp.pages;

public interface Page {

    String baseUrl();
}
