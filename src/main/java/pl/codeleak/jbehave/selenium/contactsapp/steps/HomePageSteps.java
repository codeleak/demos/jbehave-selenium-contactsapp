package pl.codeleak.jbehave.selenium.contactsapp.steps;

import org.jbehave.core.annotations.*;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ContactsApp;
import pl.codeleak.jbehave.selenium.contactsapp.pages.Dialog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HomePageSteps {

    private final ContactsApp contactsApp;
    private Dialog dialog;

    private enum DialogType {
        about, login, signup
    }

    public HomePageSteps(ContactsApp contactsApp) {
        this.contactsApp = contactsApp;
    }

    @Given("user is at home page")
    public void navigatesToHomePage() {
        contactsApp.navigateTo();
    }

    @Then("url ends with '$urlSegment'")
    public void currentUrlEndsWith(String urlSegment) {
        assertThat(contactsApp.getCurrentUrl(), endsWith(urlSegment));
    }

    @Then("page title is '$title'")
    public void pageTitleIs(String title) {
        assertThat(contactsApp.getTitle(), equalTo(title));
    }

    @Then("toolbar title is '$title'")
    public void toolbarTitleIs(String title) {
        assertThat(contactsApp.getToolbarTitle(), equalTo(title));
    }

    @Then("header text contains '$headingText'")
    public void headerTextContains(String headingText) {
        assertThat(contactsApp.getHeading(), containsString(headingText));
    }

    @Then("sub header text contains '$subHeadingText'")
    public void subHeaderTextContains(String subHeadingText) {
        assertThat(contactsApp.getSubHeading(), containsString(subHeadingText));
    }

    @Then("footer with copy contains current year")
    public void footerContainsCurrentYear() {
        // application bug
        assertThat(contactsApp.getFooter().getText(), startsWith("©2019"));
    }

    @When("user opens '$dialogType' dialog")
    public void userOpensDialog(DialogType dialogType) {
        switch (dialogType) {
            case about:
                dialog = contactsApp.openAboutDialog();
                break;
            case login:
                dialog = contactsApp.openLoginDialog();
                break;
            case signup:
                dialog = contactsApp.openSignupDialog();
                break;
        }
    }

    @Then("'$dialogType' dialog is visible")
    public void dialogIsVisible(DialogType dialogType) {
        assertThat(dialog.isVisible(), is(true));
    }

    @When("user closes '$dialogType' dialog")
    public void closeDialog(DialogType dialogType) {
        dialog.closeDialog();
    }

    @Then("'$dialogType' dialog is not visible")
    public void dialogIsNotVisible(DialogType dialogType) {
        assertThat(dialog.isVisible(), is(false));
    }
}

