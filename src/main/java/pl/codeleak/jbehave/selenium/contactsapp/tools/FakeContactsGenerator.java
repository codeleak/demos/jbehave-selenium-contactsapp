package pl.codeleak.jbehave.selenium.contactsapp.tools;

import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FakeContactsGenerator {
    public static void main(String[] args) {
        Faker faker = new Faker();
        var contacts = IntStream.rangeClosed(1, 10).boxed().map(i -> {
                    Name fakeName = faker.name();
                    String email = fakeName.firstName().toLowerCase() + "." + fakeName.lastName().toLowerCase() + "@" + faker.company().url().replaceAll("www.", "");
                    String phone = faker.phoneNumber().cellPhone();
                    if (faker.bool().bool()) {
                        return new GeneratedContact(i, fakeName.fullName(), email, phone, GeneratedContact.label(faker.random().nextInt(0, 2)));
                    } else {
                        return new GeneratedContact(i, fakeName.fullName(), email, phone);
                    }
                }
        ).collect(Collectors.toUnmodifiableList());

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println(gson.toJson(contacts));
    }


    public static class GeneratedContact {
        private final int id;
        private final String name;
        private final String email;
        private final String phone;
        private final String notes = "";
        private final boolean starred = false;
        private final String avatarUrl = "";
        private final List<String> labels;

        public GeneratedContact(int id, String name, String email, String phone, String... label) {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone = phone;
            this.labels = List.of(label);
        }

        public static String label(int i) {
            return List.of("Friend", "Family", "Co-worker").get(i);
        }

    }
}
