package pl.codeleak.jbehave.selenium.contactsapp;

import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.junit.JUnitStory;
import pl.codeleak.jbehave.selenium.contactsapp.support.WebDriverEmbedder;

public abstract class ContactsAppStory extends JUnitStory {
    @Override
    public Embedder configuredEmbedder() {
        return new WebDriverEmbedder();
    }
}
