package pl.codeleak.jbehave.selenium.contactsapp.steps;

import org.jbehave.core.annotations.*;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ContactsApp;
import pl.codeleak.jbehave.selenium.contactsapp.pages.login.LoginDialog;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class LoginSteps {

    private final ContactsApp contactsApp;
    private LoginDialog loginDialog;

    public LoginSteps(ContactsApp contactsApp) {
        this.contactsApp = contactsApp;
    }

    @Given("login dialog is displayed")
    public void loginDialogIsDisplayed() {
        loginDialog = contactsApp.navigateTo().openLoginDialog();
    }

    @When("user enters '$username' and '$password'")
    public void entersUsernameAndPassword(String username, String password) {
        loginDialog.username(username).password(password);
    }

    @When("user clicks 'Log in'")
    public void clickLogin() {
        loginDialog.login();
    }

    @Then("user is taken to the contact list page")
    public void contactPageIsShown() {
        assertThat(loginDialog.contactsPage().getToolbarTitle(), containsString("Contacts"));
    }

    @Then("user sees snackbar with message containing '$message'")
    public void snackbarContains(String message) {
        assertThat(loginDialog.snackbar().getText(), containsString(message));
    }

    @Then("user sees $usernameError and/or $passwordError")
    public void formErrors(String usernameError, String passwordError) {
        assertThat(loginDialog.getErrorMessages(), hasItems(usernameError, passwordError));
    }

}
