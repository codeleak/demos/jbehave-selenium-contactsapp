package pl.codeleak.jbehave.selenium.contactsapp.pages;

public interface Dialog {

    boolean isVisible();

    String getDialogTitle();

    void closeDialog();
}
