package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import pl.codeleak.jbehave.selenium.contactsapp.support.ApplicationProperties;

public abstract class AbstractComponent extends WebDriverPage implements Component {

    private WebDriverWait wait;

    public AbstractComponent(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    @Override
    public WebDriverWait webDriverWait() {
        if (wait == null) {
            wait = new WebDriverWait(getDriverProvider().get(), ApplicationProperties.getWaitTimeout());
        }
        return wait;
    }

    @Override
    public void clearAndSendKeys(WebElement input, String value) {
        input.click();
        var valLen = input.getAttribute("value").length();
        for (int i = 0; i < valLen; i++) {
            input.sendKeys(Keys.BACK_SPACE);
        }
        input.sendKeys(value);
    }

    @Override
    public boolean isElementPresent(By locator) {
        try {
            findElement(locator);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Override
    public boolean isElementVisible(By locator) {
        return findElement(locator).isDisplayed();
    }

}
