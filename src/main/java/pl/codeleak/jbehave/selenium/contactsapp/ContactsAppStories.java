package pl.codeleak.jbehave.selenium.contactsapp;

import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.JUnitStories;
import pl.codeleak.jbehave.selenium.contactsapp.support.WebDriverEmbedder;

import java.util.List;

import static java.util.Arrays.asList;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;

public class ContactsAppStories extends JUnitStories {

    @Override
    public Embedder configuredEmbedder() {
        return new WebDriverEmbedder();
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder()
                .findPaths(codeLocationFromClass(this.getClass()).getFile(), asList("**/*.story"), null);
    }

}
