package pl.codeleak.jbehave.selenium.contactsapp.pages.contacts;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractDialog;

public class ContactDetailsDialog extends AbstractDialog {

    public ContactDetailsDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public String getName() {
        return findElement(By.cssSelector("#contact-details span[data-property=contact-name]")).getText().trim();
    }

    public String getEmail() {
        return findElement(By.xpath("//div[@id = 'contact-details']//i[text() = 'mail']/following::span[@class='subheading']")).getText().trim();
    }

    public String getPhone() {
        return findElement(By.xpath("//div[@id = 'contact-details']//i[text() = 'phone']/following::span[@class='subheading']")).getText().trim();
    }
}
