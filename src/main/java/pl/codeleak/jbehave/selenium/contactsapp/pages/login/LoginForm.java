package pl.codeleak.jbehave.selenium.contactsapp.pages.login;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractPage;
import pl.codeleak.jbehave.selenium.contactsapp.pages.ContactsApp;
import pl.codeleak.jbehave.selenium.contactsapp.pages.contacts.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElementsLocatedBy;

public abstract class LoginForm extends AbstractPage {

    protected WebElement loginForm;

    private ContactsPage contactsPage;
    private ContactFormDialog contactFormDialog;
    private ContactDetailsDialog contactDetailsDialog;

    public LoginForm(WebDriverProvider driverProvider) {
        super(driverProvider);
        loginForm = findElement(By.id("login-form"));
    }

    public ContactsApp cancel() {
        var cancel = loginForm.findElement(By.cssSelector("#login-form-actions > button.v-btn.primary--text"));
        cancel.click();
        return new ContactsApp(getDriverProvider());
    }

    public LoginForm username(String username) {
        var input = loginForm.findElement(By.id("username"));
        input.clear();
        input.sendKeys(username);
        return this;
    }

    public LoginForm password(String password) {
        var input = loginForm.findElement(By.id("password"));
        input.clear();
        input.sendKeys(password);
        return this;
    }

    public LoginForm login() {
        var submitButton = loginForm.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();
        return this;
    }

    public ContactsPage contactsPage() {
        return new ContactsPage(getDriverProvider());
    }

    public List<String> getErrorMessages() {
        // wait until error messages are visible
        List<WebElement> errorElements = webDriverWait()
                .until(visibilityOfAllElementsLocatedBy(By.cssSelector("#login-form .v-text-field__details .error--text .v-messages__message")));
        // get all error messages
        List<String> errors = errorElements.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
        return errors;
    }
}
