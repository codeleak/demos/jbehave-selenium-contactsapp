package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;

public class Footer extends AbstractComponent {

    public Footer(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public String getText() {
        return findElement(By.tagName("footer")).getText().trim();
    }
}
