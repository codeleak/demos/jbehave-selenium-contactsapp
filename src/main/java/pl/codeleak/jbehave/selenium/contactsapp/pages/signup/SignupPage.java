package pl.codeleak.jbehave.selenium.contactsapp.pages.signup;

import org.jbehave.web.selenium.WebDriverProvider;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractPage;

public class SignupPage extends AbstractPage {
    public SignupPage(WebDriverProvider driverProvider) {
        super(driverProvider);
    }
}
