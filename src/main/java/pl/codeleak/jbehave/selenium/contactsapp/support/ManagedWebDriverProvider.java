package pl.codeleak.jbehave.selenium.contactsapp.support;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.jbehave.web.selenium.DelegatingWebDriverProvider;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ManagedWebDriverProvider extends DelegatingWebDriverProvider {

    private static final Map<String, Class<? extends RemoteWebDriver>> drivers = Map.of(
            "chrome", ChromeDriver.class,
            "firefox", FirefoxDriver.class);

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagedWebDriverProvider.class);

    private static final int DEFAULT_TIMEOUT_IN_MILLIS = 100;

    @Override
    public void initialize() {
        delegate.set(newRemoteWebDriver(ApplicationProperties.getBrowser()));
    }

    private RemoteWebDriver newRemoteWebDriver(String browser) {
        Class<? extends RemoteWebDriver> driverClass = drivers.get(browser);

        if (driverClass == null) {
            throw new IllegalArgumentException("Not supported browser " + browser);
        }

        WebDriverManager.getInstance(driverClass).setup();
        RemoteWebDriver remoteWebDriver = browser.equals("chrome") ? chrome() : firefox();
        remoteWebDriver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT_IN_MILLIS, TimeUnit.MILLISECONDS);
        remoteWebDriver.manage().window().maximize();
        return remoteWebDriver;
    }

    private RemoteWebDriver firefox() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", ApplicationProperties.getDownloadDirectory().toString());
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setProfile(profile);
        return new FirefoxDriver(firefoxOptions);
    }

    private RemoteWebDriver chrome() {
        Map<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("download.default_directory", ApplicationProperties.getDownloadDirectory().toString());
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("prefs", chromePrefs);
        return new ChromeDriver(chromeOptions);
    }

}
