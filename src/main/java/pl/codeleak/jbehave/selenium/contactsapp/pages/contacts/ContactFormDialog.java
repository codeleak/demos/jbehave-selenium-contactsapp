package pl.codeleak.jbehave.selenium.contactsapp.pages.contacts;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.*;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractDialog;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class ContactFormDialog extends AbstractDialog {

    private final WebElement form;

    ContactFormDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
        this.form = webDriverWait().until(visibilityOfElementLocated(By.id("contact-form")));
    }

    public void setEmail(String value) {
        var email = form.findElement(By.cssSelector("input[name=contact-email]"));
        email.clear();
        email.sendKeys(value);
    }

    public void setName(String value) {
        var input = form.findElement(By.cssSelector("input[name=contact-name]"));
        clearAndSendKeys(input, value);
    }

    public void setPhone(String value) {
        var input = form.findElement(By.cssSelector("input[name=contact-phone]"));
        clearAndSendKeys(input, value);
    }

    public void setNotes(String value) {
        var input = form.findElement(By.cssSelector("input[name=contact-notes]"));
        clearAndSendKeys(input, value);
    }

    public void addLabel(String value) {
        var labels = form.findElement(By.cssSelector("div.v-select__slot > div.v-select__selections > input"));
        labels.sendKeys(value);
        labels.sendKeys(Keys.ENTER);
        labels.sendKeys(Keys.ESCAPE);
    }

    public void setAvatar(String resourceName) {
        var fileInput = findElement(By.cssSelector("input[type=file]"));
        var file = new File(getClass().getResource(resourceName).getFile());
        fileInput.sendKeys(file.getAbsolutePath());
        webDriverWait().until(webDriver -> webDriver.findElement(By.cssSelector("#contact-form .v-avatar .v-image__image.v-image__image--cover")).getCssValue("background-image").contains("data:image/"));
    }

    public void save() {
        form.findElement(By.cssSelector("button[type=submit]")).click();
    }

    public List<String> getErrorMessages() {
        var errorElements = webDriverWait().until(visibilityOfAllElementsLocatedBy(By.cssSelector("#contact-form .v-text-field__details .error--text .v-messages__message")));
        return errorElements.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public String getAvatarImage() {
        return webDriverWait().until(driver -> form.findElement(By.cssSelector(".v-avatar .v-image__image.v-image__image--cover")).getCssValue("background-image"));
    }

    public String getContactName() {
        return form.findElement(By.cssSelector("input[name=contact-name]")).getAttribute("value");
    }

    public String getContactEmail() {
        return form.findElement(By.cssSelector("input[name=contact-email]")).getAttribute("value");
    }

    public String getContactPhone() {
        return form.findElement(By.cssSelector("input[name=contact-phone]")).getAttribute("value");
    }

    public String getNotes() {
        return form.findElement(By.cssSelector("input[name=contact-notes]")).getAttribute("value");
    }

    public List<String> getLabels() {
        var chips = form.findElements(By.cssSelector(".v-chip"));
        return chips.stream().map(WebElement::getText).collect(Collectors.toUnmodifiableList());
    }

}
