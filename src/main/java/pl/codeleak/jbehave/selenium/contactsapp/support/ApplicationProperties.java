package pl.codeleak.jbehave.selenium.contactsapp.support;

import org.seleniumhq.jetty9.io.RuntimeIOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public final class ApplicationProperties {

    private static final String APPLICATION_PROPERTIES = "/application.properties";

    public static final String BASE_URL_PROPERTY = "baseUrl";
    public static final String DEFAULT_BASE_URL = "https://qalabs.gitlab.io/vuejs-contacts-demo/";

    public static final String BROWSER_PROPERTY = "browser";
    public static final String DEFAULT_BROWSER = "firefox";

    public static final String WAIT_TIMEOUT_PROPERTY = "waitTimeout";
    public static final String DEFAULT_WAIT_TIMEOUT = "2";

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationProperties.class);
    private static final Properties properties = new Properties();

    static {
        try {
            InputStream inputStream = ApplicationProperties.class.getResourceAsStream(APPLICATION_PROPERTIES);
            properties.load(inputStream);

            LOGGER.info("Resolved application properties");
            LOGGER.info(BROWSER_PROPERTY + ": " + getBrowser());
            LOGGER.info(BASE_URL_PROPERTY + ": " + getBaseUrl());
            LOGGER.info(WAIT_TIMEOUT_PROPERTY + ": " + getWaitTimeout());

        } catch (IOException e) {
            throw new RuntimeIOException("Can't read properties from " + APPLICATION_PROPERTIES, e);
        }
    }

    public static String getBrowser() {
        return resolvePropertyValue(BROWSER_PROPERTY, DEFAULT_BROWSER);
    }

    public static Path getDownloadDirectory() {
        return Paths.get("target").toAbsolutePath();
    }

    public static String getBaseUrl() {
        return resolvePropertyValue(BASE_URL_PROPERTY, DEFAULT_BASE_URL);
    }

    public static int getWaitTimeout() {
        return Integer.parseInt(resolvePropertyValue(WAIT_TIMEOUT_PROPERTY, DEFAULT_WAIT_TIMEOUT));
    }

    private static String resolvePropertyValue(String propertyKey, String defaultValue) {
        if (propertyKey != null && !propertyKey.isEmpty()) {
            return System.getProperty(propertyKey, properties.getProperty(propertyKey));
        }
        return defaultValue;
    }
}