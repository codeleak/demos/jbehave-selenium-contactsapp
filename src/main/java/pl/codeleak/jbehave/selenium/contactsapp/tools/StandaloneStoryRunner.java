package pl.codeleak.jbehave.selenium.contactsapp.tools;

import pl.codeleak.jbehave.selenium.contactsapp.support.WebDriverEmbedder;

import java.util.List;

public class StandaloneStoryRunner {

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new RuntimeException("Provide story name as an argument, e.g. `user_can_browse_home_page.story`");
        }
        var storiesBasePath = "pl/codeleak/jbehave/selenium/contactsapp/stories/";
        var storyToRun = storiesBasePath + args[0];
        new WebDriverEmbedder().runStoriesAsPaths(List.of(storyToRun));
    }
}
