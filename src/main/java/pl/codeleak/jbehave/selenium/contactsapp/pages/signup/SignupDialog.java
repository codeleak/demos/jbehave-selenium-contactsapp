package pl.codeleak.jbehave.selenium.contactsapp.pages.signup;

import org.jbehave.web.selenium.WebDriverProvider;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractDialog;

public class SignupDialog extends AbstractDialog {
    public SignupDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
    }
}

