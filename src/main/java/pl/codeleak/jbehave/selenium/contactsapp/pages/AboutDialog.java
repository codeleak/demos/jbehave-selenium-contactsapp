package pl.codeleak.jbehave.selenium.contactsapp.pages;

import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.*;

import java.util.List;
import java.util.stream.Collectors;

public class AboutDialog extends AbstractDialog {

    AboutDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
    }

    public List<String> getFeaturesAsList() {
        var listItems = dialog.findElements(By.cssSelector("p + ul > li")); // search within dialog
        return listItems.stream()
                .map(webElement -> webElement.getText().trim())
                .collect(Collectors.toUnmodifiableList());
    }
}
