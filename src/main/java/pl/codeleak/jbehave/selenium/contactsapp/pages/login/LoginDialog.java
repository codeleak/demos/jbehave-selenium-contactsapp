package pl.codeleak.jbehave.selenium.contactsapp.pages.login;

import org.jbehave.web.selenium.WebDriverProvider;
import pl.codeleak.jbehave.selenium.contactsapp.pages.AbstractDialog;
import pl.codeleak.jbehave.selenium.contactsapp.pages.Dialog;

public class LoginDialog extends LoginForm implements Dialog {

    private final Dialog dialogDelegate;

    public LoginDialog(WebDriverProvider driverProvider) {
        super(driverProvider);
        dialogDelegate = new LoginDialogDelegate(driverProvider);
    }

    @Override
    public boolean isVisible() {
        return dialogDelegate.isVisible();
    }

    public String getDialogTitle() {
        return dialogDelegate.getDialogTitle();
    }

    @Override
    public void closeDialog() {
        dialogDelegate.closeDialog();
    }

    // no multi-inheritance hence a simple trick with delegate
    private static class LoginDialogDelegate extends AbstractDialog {
        public LoginDialogDelegate(WebDriverProvider driverProvider) {
            super(driverProvider);
        }
    }
}
