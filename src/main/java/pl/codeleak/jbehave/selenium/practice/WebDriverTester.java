package pl.codeleak.jbehave.selenium.practice;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverTester {

    WebDriver webDriver;

    @Before
    public void before() {
        WebDriverManager.firefoxdriver().setup();
        webDriver = new FirefoxDriver();
    }

    @After
    public void after() {
        webDriver.quit();
    }

    @Test
    public void tester() {
        webDriver.get("http://localhost:8080/vuejs-contacts-demo");
    }

    @Test
    public void examples() {
        webDriver.get("http://localhost:8080/vuejs-contacts-demo/#/login");

        WebElement webElement = webDriver.findElement(By.id("username"));
        System.out.println(webElement.isEnabled());

        webElement.click();
        webElement.clear();
        webElement.sendKeys("some_value");

        System.out.println(webElement.getAttribute("value"));
        System.out.println(webElement.getAttribute("name"));
        System.out.println(webElement.getAttribute("required"));
    }

}
