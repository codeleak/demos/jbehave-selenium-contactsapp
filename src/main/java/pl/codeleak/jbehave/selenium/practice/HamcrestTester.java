package pl.codeleak.jbehave.selenium.practice;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

// http://hamcrest.org/JavaHamcrest/tutorial
public class HamcrestTester {

    @Test
    public void tester() {

    }

    @Test
    public void examples() {
        // string matching
        assertThat("String 1", equalTo("String 1"));
        assertThat("String 1", equalToIgnoringCase("string 1"));
        assertThat("String 1", containsString("1"));
        assertThat("String 1", startsWith("Str"));
        assertThat("String 1", endsWith(" 1"));

        // logical operator example
        assertThat("String 1", allOf(startsWith("Str"), endsWith(" 1")));


        var list1 = List.of("1", "2");

        // verify list1 has items
        assertThat(list1, hasItems("1", "2"));
        assertThat(list1, hasItems("2", "1"));
        assertThat(list1, hasItems("1"));

        // verify list1 contains elements of list2
        assertThat(list1, containsInAnyOrder(List.of("2", "1").toArray()));

        var list2 = List.of("String 1", "String 2");
        assertThat(list2, hasItem(containsString("tri")));

    }
}
