package pl.codeleak.jbehave.selenium.practice.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HomePageTests {

    private WebDriver webDriver;

    @Before
    public void before() {
        WebDriverManager.firefoxdriver().setup();
        webDriver = new FirefoxDriver();
    }

    @After
    public void after() {
        webDriver.quit();
    }

    @Test
    public void homePageIsLoadedProperly() {
        // user is at home page
        webDriver.get("http://localhost:8080/vuejs-contacts-demo");
        // url ends with '#/home'
        assertThat(webDriver.getCurrentUrl(), endsWith("#/home"));
    }

    @Test
    public void homePageIsDisplayedCorrectly() {
        // user is at home page
        webDriver.get("http://localhost:8080/vuejs-contacts-demo");

        // page title is 'Contacts App'
        assertThat(webDriver.getTitle(), equalTo("Contacts App"));

        // toolbar title is 'Contacts App'
        assertThat(webDriver.findElement(By.cssSelector("nav div.v-toolbar__title")).getText(), equalTo("Contacts App"));

        // header text contains 'Manage your contacts with ease!'
        assertThat(webDriver.findElement(By.cssSelector("#home h1")).getText(), containsString("Manage your contacts with ease!"));

        // sub header text contains 'Contacts App is a demo SPA application created in VueJS.'
        assertThat(webDriver.findElement(By.cssSelector("#home h1 + p")).getText(), containsString("Contacts App is a demo SPA application created in VueJS."));

        // footer with copy contains 2019
        assertThat(webDriver.findElement(By.tagName("footer")).getText(), containsString("2019"));
    }

}
