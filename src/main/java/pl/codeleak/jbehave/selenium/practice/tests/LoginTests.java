package pl.codeleak.jbehave.selenium.practice.tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LoginTests {
    private WebDriver webDriver;

    @Before
    public void before() {
        WebDriverManager.firefoxdriver().setup();
        webDriver = new FirefoxDriver();
    }

    @After
    public void after() {
        webDriver.quit();
    }

    @Test
    public void userLogsInCorrectly() {
        // user is at login page
        webDriver.get("http://localhost:8080/vuejs-contacts-demo/#/login");

        // user enters 'contacts' and 'demo'
        var usernameField = webDriver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys("contacts");

        var passwordField = webDriver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys("demo");

        // user clicks 'Log in'
        var submitButton = webDriver.findElement(By.cssSelector("button[type='submit']"));
        submitButton.click();

        // user is taken to the contact list page
        assertThat(webDriver.findElement(By.cssSelector("nav div.v-toolbar__title")).getText(), equalTo("Contacts"));
    }
}
