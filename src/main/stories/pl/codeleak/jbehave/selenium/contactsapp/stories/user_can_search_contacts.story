User can search contacts

Scenario: Search with no results
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-25.json'
Then user should see exactly 5 contacts
When user selects to see 'All' records
Then user should see exactly 25 contacts
When user enters 'impossible_to_find' in search
Then user sees no results message: 'No results for current filters.'

Scenario: Search with exact results (1)
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-25.json'
Then user should see exactly 5 contacts
When user selects to see 'All' records
Then user should see exactly 25 contacts
When user enters '.biz' in search
Then user should see exactly 7 contacts
And user should see contact names specified by:
|name|
|Mindy Davis|
|Shanta Little II|
|Son Franecki|
|Grisel Turner|
|Keven Smitham|
|Sylvie Weimann|
|Fritz Shanahan DDS|

Scenario: Search with exact results (2)
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-25.json'
Then user should see exactly 5 contacts
When user selects to see 'All' records
Then user should see exactly 25 contacts
When user enters '.com' in search
Then user should see exactly 3 contacts
And user should see contact names and emails specified by:
|name|email|
|Sarah West|claud.greenfelder@ernser-cremin.com|
|Miss Mikki Franecki|val.purdy@shieldsandsons.com|
|Coleman Bogan DDS|cassy.torp@bashiriangroup.com|
