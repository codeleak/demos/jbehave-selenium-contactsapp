User can delete selected contact(s)

Scenario: Delete single contact
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
Then user should see contact with name 'Alonso MacGyver'
When user clicks delete icon for contact with name 'Alonso MacGyver'
Then user sees alert with message 'Are you sure you want to delete this contact?'
When user confirms deletion
Then user should not see contact with name 'Alonso MacGyver'

Scenario: Delete all selected contacts
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user toggles select all contacts
And user clicks delete selected contacts
Then user sees alert with message 'Are you sure you want to delete selected contacts?'
When user confirms deletion
Then user sees no results message: 'No data.'

Scenario: Delete selected contacts
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user toggles select contact with name 'Ahmed Block'
And user toggles select contact with name 'Owen Kunze'
And user clicks delete selected contacts
Then user sees alert with message 'Are you sure you want to delete selected contacts?'
When user confirms deletion
Then user should see exactly 3 contacts
And user should not see contact with name 'Ahmed Block'
And user should not see contact with name 'Owen Kunze'
