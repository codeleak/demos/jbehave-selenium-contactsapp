User can download contacts

Scenario: Download all contacts
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user toggles select all contacts
And user clicks download contacts
Then downloaded file should contain lines specified by:
|name|
|Name,Email,Phone,Labels,Favorite|
|"Ahmed Block",samara.reynolds@wildermaninc.io,804-713-5070,"",false|
|"Launa Bogan Sr.",zoila.goyette@bahringerrodriguezandanderson.co,1-503-283-0488,"",false|
|"Owen Kunze",emerson.marquardt@oreillycorwinandleffler.info,(810) 226-8041,"",false|
|"Alonso MacGyver",,564-319-2491,"",false|
|"Dorsey Cronin",ricardo.hilpert@leschinc.name,(617) 418-9151,"",false|

Scenario: Download selected contacts
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user toggles select contact with name 'Ahmed Block'
And user toggles select contact with name 'Owen Kunze'
And user clicks download contacts
Then downloaded file should contain lines specified by:
|name|
|Name,Email,Phone,Labels,Favorite|
|"Ahmed Block",samara.reynolds@wildermaninc.io,804-713-5070,"",false|
|"Owen Kunze",emerson.marquardt@oreillycorwinandleffler.info,(810) 226-8041,"",false|