User can modify contact

Scenario: Modify selected contact's email
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user opens edit contact form for 'Alonso MacGyver'
Then user sees name: 'Alonso MacGyver' in the form
And user sees email: '' in the form
When user enters email: 'ivory.steuber@dach-monahan.org'
And user saves the form
Then contact form dialog should be closed
Then user should see contact with name 'Alonso MacGyver'
And user should see contact with email 'ivory.steuber@dach-monahan.org'
