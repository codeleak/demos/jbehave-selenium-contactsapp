User can add or edit contact's avatar

Scenario: Create contact with avatar
!-- Meta:
!-- @skip
Given there are no contacts
When user opens new contact form dialog
And user enters name: 'Alonso MacGyver'
When user uploads avatar: '/avatar-sample.jpg'
Then user sees avatar: 'data:image/jpeg;base64' in the form
When user saves the form
Then contact form dialog should be closed
And user should see contact with name 'Alonso MacGyver'

Scenario: Modify contact avatar
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
When user opens edit contact form for 'Alonso MacGyver'
Then user sees name: 'Alonso MacGyver' in the form
When user uploads avatar: '/avatar-sample.jpg'
Then user sees avatar: 'data:image/jpeg;base64' in the form
When user saves the form
Then contact form dialog should be closed
And user should see contact with name 'Alonso MacGyver'
