User can create contact

Scenario: Create contact with name only
!-- Meta:
!-- @skip
Given there are no contacts
When user opens new contact form dialog
And user enters name: 'John Doe'
And user saves the form
Then contact form dialog should be closed
And user should see contact with name 'John Doe'

Scenario: Create contact with more fields
!-- Meta:
!-- @skip
Given there are no contacts
When user opens new contact form dialog
And user enters name: 'John Doe'
And user enters email: 'john.doe@demo.example'
And user enters phone: '(123) 321 123'
And user enters notes: 'Some random notes'
And user adds label: 'WIP'
And user adds label: 'Friend'
And user saves the form
Then contact form dialog should be closed
And user should see contact with name 'John Doe'

Scenario: User can NOT save the empty form
!-- Meta:
!-- @skip
Given there are no contacts
When user opens new contact form dialog
And user saves the form
Then user sees 'Name is required.' error in the contact form

Scenario: User can NOT save form with no name and invalid email
!-- Meta:
!-- @skip
Given there are no contacts
When user opens new contact form dialog
And user enters email: 'no_email'
And user saves the form
Then user sees 'Name is required.' error in the contact form
And user sees 'Email must be a valid email address' error in the contact form