User can browse contacts

Scenario: No results
!-- Meta:
!-- @skip
Given there are no contacts
Then user sees no results message: 'No data.'

Scenario: 5 results are shown
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
Then user should see exactly 5 contacts
And user should see contact with name 'Ahmed Block'
And user should see contact with name 'Launa Bogan Sr.'
And user should see contact with name 'Owen Kunze'
And user should see contact with name 'Alonso MacGyver'
And user should see contact with name 'Dorsey Cronin'

Scenario: All results
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-25.json'
Then user should see exactly 5 contacts
When user selects to see 'All' records
Then user should see exactly 25 contacts

Scenario: Pagination
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-25.json'
Then user should see exactly 5 contacts
When user selects to see '10' records
Then user should see exactly 10 contacts
And next page is enabled
And previous page is disabled
When user navigates to next page
Then user should see exactly 10 contacts
And next page is enabled
And previous page is enabled
When user navigates to next page
Then user should see exactly 5 contacts
And next page is disabled
And previous page is enabled
When user navigates to previous page
Then user should see exactly 10 contacts
And next page is enabled
And previous page is enabled
When user navigates to previous page
Then user should see exactly 10 contacts
And next page is enabled
And previous page is disabled