User can see selected contact's details

Scenario: Get details
!-- Meta:
!-- @skip
Given there are contacts as described in '/contacts-5.json'
Then user should see contact with name 'Launa Bogan Sr.'
When user clicks name of contact 'Launa Bogan Sr.'
Then contact details dialog is visible
And user sees contact name: 'Launa Bogan Sr.'
And user sees contact email 'zoila.goyette@bahringerrodriguezandanderson.co' next to email icon
And user sees contact phone '1-503-283-0488' next to phone icon