User can browser home page

Scenario: Home page is loaded properly
!-- Meta:
!-- @skip
Given user is at home page
Then url ends with '#/home'

Scenario: Home page is displayed correctly
!-- Meta:
!-- @skip
Given user is at home page
Then page title is 'Contacts App'
And toolbar title is 'Contacts App'
And header text contains 'Manage your contacts with ease!'
And sub header text contains 'Contacts App is a demo SPA application created in VueJS.'
And footer with copy contains current year

Scenario: User opens and closes signup dialog
!-- Meta:
!-- @skip
Given user is at home page
When user opens 'signup' dialog
Then 'signup' dialog is visible
When user closes 'signup' dialog
Then 'signup' dialog is not visible

Scenario: User opens and closes login dialog
!-- Meta:
!-- @skip
Given user is at home page
When user opens 'login' dialog
Then 'login' dialog is visible
When user closes 'login' dialog
Then 'login' dialog is not visible