User can login

Scenario: User logs in correctly
!-- Meta:
!-- @skip
Given login dialog is displayed
When user enters 'contacts' and 'demo'
And user clicks 'Log in'
Then user is taken to the contact list page

Scenario: User does NOT provide correct credentials
!-- Meta:
!-- @skip
Given login dialog is displayed
When user enters '<username>' and '<password>'
And user clicks 'Log in'
Then user sees snackbar with message containing 'Invalid credentials. Please use "contacts" and "demo" to login.'

Examples:
|username|password|
|john|password|
|jane|pwd|

Scenario: User sees validation errors
!-- Meta:
!-- @skip
Given login dialog is displayed
When user enters '<username>' and '<password>'
And user clicks 'Log in'
Then user sees <username_error> and/or <password_error>

Examples:
|username|password|username_error|password_error|
|||Username is required.|Password is required.|
|j|p|Username must be at least 3 characters long|Password must be at least 3 characters long|
|jo|pa|Username must be at least 3 characters long|Password must be at least 3 characters long|
