JBehave and Selenium
====================

ContactsApp automated testing with JBehave and Selenium WebDriver

## Prerequisites

- Git
- JDK 11+
- IntelliJ with JBehave Plugin (https://plugins.jetbrains.com/plugin/7268-jbehave-support)
- Terminal
- Firefox and Chrome browsers

## Install ContactsApp locally

Follow the instructions here: https://gitlab.com/qalabs/vuejs-contacts-demo. 

> Note: You are able build and run the application either with Maven or with NodeJS.

## Run stories in terminal

- Open Terminal
- Run `git clone <repository url>`
- Navigate to `jbehave-selenium-softwaremind`
- Run `./mvnw clean test`

> Note: The stories will be executed in `Firefox` browser by default. To change the browser to `Chrome` run: `./mvnw clean test -Dbrowser=chrome`

## Running individual stories in IntelliJ

- Open IntelliJ
- Import Maven project
- Run one of Java files located in `pl.codeleak.samples.stories` package

## Adding a new individual story

- Create `my-story.story` file in `src/main/stories/pl/codeleak/samples/jbehave/selenium/contactsapp/stories`
- Optionally create a `Java` class of the `MyStory.java` extending from `pl.codeleak.jbehave.selenium.contactsapp.ContactsAppStory` in `src/main/java/pl/codeleak/samples/jbehave/selenium/contactsapp/stories`
- Run an individual story via newly created `Java` class or run all stories using `pl.codeleak.jbehave.selenium.contactsapp.ContactsAppStories`
- Stories or scenarios annotated with `@skip` will be skipped while running

## Application properties

- You can pass properties as system properties: `-D`. System properties override application properties.
- You can change properties in `src/main/resources/application.properties`

See  `src/main/resources/application.properties`