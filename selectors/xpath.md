XPath Selectors
===============

## How to verify selectors?

- Open https://qalabs.gitlab.io/vuejs-contacts-demo and login
- Open Chrome or Firefox dev tools
- Execute selectors using `$x` function in JavaScript console: `$x("")`

## Selecting nodes

- `nodename` - selects all nodes with the name `nodename`
- `/` - selects from the root node
- `//` - selects nodes in the document from the current node that match the selection no matter where they are
- `.` - selects the current node 
- `..` - selects the parent of the current node
- `*` - matches any element node
- `@*` - matches any attribute node

## Useful selectors

- `//*[@id]` - match all elements with `id` attribute set
- `//*[@id][@type]` - match all elements with `id` and `type` attributes set
- `//input[@type='text' and @role='combobox']` - match all `input` with `type` and `role` attributes
- `//input[@type='text' or @type='checkbox']` - match all `input` with `type=text` or `text=checkbox`
- `//*[@id='contacts-list']` - match all elements with `contacts-list` id
- `//*[@id='contacts-list']//table/tbody/tr` - match all `tr`
- `//table/tbody/tr[1]` - match first `tr`
- `//table/tbody/tr[last()]` - match last `tr`
- `//table/tbody/tr[last()]/td[3]` - match third `td` in the last `tr`
- `//tbody | //thead` - match all `tbody` and `thead`
- `count(//td)` - return number of `td` in document
- `//*[contains(text(), 'Friends')]` - match all nodes containing given text
- `//*[text() = 'Friends']` - match all nodes with given text
- `//button//div[contains(text(), 'New Contact')]` - match all `div` containing text
- `//button//div[contains(text(), 'New Contact')]/..` - match all `button` with `div` containing text
- `//button//div[contains(text(), 'New Contact')]/parent::button` - match all `button` with `div` containing text
- `//button//div[contains(text(), 'New Contact')]/ancestor::div[@class='v-toolbar__content']` - match all `div` that are ancestors of `div` with text
- `//*[@class='v-list__tile__title'][contains(text(), 'Friends')]` - match all with `class` and containing text
- `//*[@class='v-list__tile__title' and contains(text(), 'Friends')]` - match all with `class` and containing text
- `//button[contains(@class, 'v-btn')]` - match all `button` with `class` containing text
- `//*[i='delete']` - `<div<i>delete</i></div` => `div`
- `//div[label='Password']/input` - `<div><label>Password</label><input /></div>` => `input`

## Exercises

- Match search input
- Match all `button` elements with `disabled=disabled` and `class` containing `v-btn`
- Match all emails from the contact list table. Skip header in the table.
- Match `tr` with `td` containing `Julita Walczak`
- Match all `tr` with `td` containing `Co-workers` chip
- Match second `tr` inside `tbody` in contact list table.

### More advanced

- Match username error in the login form using XPath ancestor axis (`ancestor`)